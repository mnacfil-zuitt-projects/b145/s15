

// console.log("Hello From JS");
// // error message
// console.error("Error Something");

// // warning message 
// console.warn("Ooops Warning");

// // function that will display an error

// function errorMessage () {
// 	console.error('Errorr oh no!!!!')
// }

// errorMessage();


// function greeting() {
// 	// do something or procedure
// 	console.log('Salutations from JS!');
// }
// // invocating/calling
// greeting();
// //scope

// function fullName(){
// 	// concatenate givenName and familyName
// 	console.log(`${givenName} ${familyName}`)
// }

// fullName();

// let givenName = `John`;
// let familyName = `Smith`;

// local scope
// function computeTotal() {
// 	let numA = 20;
// 	let numB = 5;
// 	// return the sum
// 	console.log(numA + numB);
// }

// computeTotal();

// functions with parameters

// function pokemon(pangalan) {
// 	console.log( `I choose you: ${pangalan}`);
// }

// pokemon('pikachu');

// functions with multiple parameters

// function addNumbers (numA, numB, numC, numD, numE){
// 	console.log(numA + numB + numC + numD + numE);
// }
// addNumbers(1,2,3,4,5);

// function createFullName (lName, fName, mName) {
// 	console.log(`${lName}, ${fName} ${mName}`);
// }

// createFullName(`Nacfil`, 'Melvin', 'A')

//using variables as Arguments

let selectPokemon = 'Ratata'
let attackA = 'Tackle';
let attackB = 'Thunderbolt'
let attackC = 'Quick Attack';

function pokemonStats(name, attack){
	console.log(`${name} use ${attack}`);
}

pokemonStats(selectPokemon, attackC);

// Note: using variables as arguments will allow us to utilize
// code reusability this is help us reduce the amount of code that we have to write down.

// the use of the "return" expression/statement - used to display the output of a function

// function returnString() {
// 	'Hello World';
// 		2 + 1;
// 	return 	'print me' ;
// }

// const functionOutput = returnString();
// console.log(functionOutput)

// function dialog() {
// 	console.log('Ooops I did it Again');
// 	console.log('Dont you know that youre toxic')
// 	 'Im a Slave for you';
// 	return console.log('Sometimes I run!')
// }
// note: any block/line of code that will come after our return statement
// is going to be ignored because it came after the end of the function execution


 // console.log(dialog());

 // Using functions as Arguments(callbackFunction)
 // function paramets can also accept other functions as their arguments

// some Complex functions use other functions as their arguments to perform
// more complicated/complex result

function argumentsaFunction() {
	console.log(`this function was passed as an argument`)
}

function invokeFunction(argumentNaFunction, pangalawangFunction) {
	argumentNaFunction();
	pangalawangFunction(selectPokemon, attackC);
}

// invoke

invokeFunction(argumentsaFunction, pokemonStats);

// IF you want to see detailes/information about a function

