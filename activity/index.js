console.log('Hello World')
// 1. Create an "activity" folder, an "index.html" file inside of it and link the "index.js" file.

// 2. Create an "index.js" file and console log the message "Hello World" to ensure that the script file is properly associated with the html file.

// 3. Create a function that will accept the first name, last name and age as arguments and print those details in the console as a single string.

// 4. Create another function that will return a value and store it in a variable.
// commit message "done with activity"
function person(firstName, lastName, age) {
	console.log(`${firstName} ${lastName} is ${age} years of age.`)
	console.log(`This was printed inside of the function`)
}

person('John', 'Smith', 30);

function returnValue (bool){
	if(bool) {
		return `The value of isMarried is: ${bool}`
	}
}

const returnBool = returnValue(true);
console.log(returnBool)